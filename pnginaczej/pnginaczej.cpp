// pnginaczej.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstddef>
#include <stdlib.h>
using namespace std;

const char* INPUT = "luna.png";
const char* OUTPUT = "PoFridge.png";
const char* XOR = "Xor.png";
const char* KEY = "Klucz.png";
const char* KEYE = "Kluczencrypted.png";
const char* KEYD = "Kluczdecrypted.png";


struct PNG_info
{
	int Width = 0;
	int Height = 0;
	int BitDepth = 0;
	int ColorType = 0;
	int CompressionMethod = 0;
	int FilterMethod = 0;
	int InterlaceMethod = 0;

};
struct RSA
{
	int64_t n = 0;
	int64_t p = 0;
	int64_t q = 0;
	int64_t e = 0;
	int64_t f = 0;
	int64_t d = 0;
	size_t size = 0;
};

void showExif(PNG_info MyPNG)
{
	cout << "Info about opened PNG file:" << endl;
	cout << "Width of file (in pixels): " << MyPNG.Width << endl;
	cout << "Height of file (in pixels): " << MyPNG.Height << endl;
	cout << "Bit depth: " << MyPNG.BitDepth << endl;
	cout << "Color type: " << MyPNG.ColorType << endl;
	cout << "Compression method: " << MyPNG.CompressionMethod << endl;
	cout << "FilterMethod: " << MyPNG.FilterMethod << endl;
	cout << "InterlaceMethod: " << MyPNG.InterlaceMethod << endl;
};

int64_t getD(int64_t __enumber, int64_t __fnumber)
{
	__enumber = __enumber % __fnumber;
	for (int ijk = 1; ijk < __fnumber; ijk++)
		if ((__enumber*ijk) % __fnumber == 1)
			return ijk;
}


string decToHex(int n)
{
	char hexNum[2];
	int i = 1;
	string Num = "";
	while (i != -1)
	{
		int temp = 0;
		temp = n % 16;

		if (temp < 10)
		{
			hexNum[i] = temp + 48;
			i--;
		}
		else
		{
			hexNum[i] = temp + 55;
			i--;
		}

		n = n / 16;
	}
	//hexNum[3] = NULL;
	Num = hexNum;

	return Num;
};
void zmienplik(int idat, int iend, char * buf) {
	int h = (iend - idat) / 2 + idat;
	buf[h] = '\x01';
}

void losujplik(size_t size) {
	fstream key;
	key.open(KEY, ios::out | ios::binary);
	for (int i = 0; i < size; i++) {
		char cch = 255 + rand() % 255;
		//char cch = '1';
		key << cch;
	}
	key.close();
};

char * openread(char* buf, const char* name,size_t size) {
	ifstream stream;
	stream.open(name, ios::in | ios::binary);
	stream.seekg(0);
	stream.read(buf, size);
	stream.close();
	return buf;
}

void xorplik(size_t size, char* buf,int idata, int iend) {
	fstream key, xorfile;
	char* bufkey=new char[size];
	char* bufxor = new char[size];
	openread(bufkey, KEY, size);
	xorfile.open(XOR, ios::out | ios::binary);
	for (int i = 0; i < idata+6; i++) {
		xorfile << buf[i];
	}
	for (int i = idata + 6; i < iend-4; i++) {
		xorfile << (char)(buf[i] ^ bufkey[i]);
	}
	for (int i = iend-4; i < size; i++) {
		xorfile << buf[i];
	}
	key.close();
	xorfile.close();
};
void xordeplik(size_t size, char* buf, int idata, int iend) {
	fstream key, xorfile;
	char* bufkey = new char[size];
	char* bufxor = new char[size];
	openread(bufkey, KEYD, size);
	xorfile.open(XOR, ios::in | ios::binary);
	xorfile.seekg(0);
	xorfile.read(bufxor, size);
	xorfile.close();
	xorfile.open(XOR, ios::out | ios::binary);

	for (int i = 0; i < idata + 6; i++) {
		xorfile << bufxor[i];
	}
	for (int i = idata + 6; i < iend - 4; i++) {
		xorfile << (char)(bufxor[i] ^ bufkey[i]);
	}
	for (int i = iend - 4; i < size; i++) {
		xorfile << bufxor[i];
	}
	key.close();
	xorfile.close();
}

int64_t modExp(int64_t basenumber, int64_t expnumber, int64_t mnumber)
{
	int64_t __answervariable = 1;
	for (int iii = 0; iii < expnumber; iii++)
	{
		__answervariable *= basenumber;
		__answervariable = __answervariable % mnumber;
	}
	return __answervariable;
}
void encryptkey(RSA pq, bool type) {
	char* buf = new char[pq.size];
	const char* name1;
	const char* name2;
	int charcount = 0;
	
	int64_t he1 = 0;
	ofstream keyen;
	if(type==false){
		 he1 = pq.e;
		 name1 = KEY;
		 name2 = KEYE;
	}
	else{ 
		he1 = pq.d; 
		name1 = KEYE;
		name2 = KEYD;
	}
	buf = openread(buf, name1, pq.size);
	keyen.open(name2, ios::out | ios::binary);
	
	for (int i = 0; i < pq.size; i++) {
		int result=1;
		for (; he1; he1 >>= 1)
		{
			if (he1 & 1) result = (1LL * result * buf[i]) % pq.n;
			buf[i] = (1LL * buf[i] * buf[i]) % pq.n;
		}
		keyen << buf[i];
	}
	
}


int hexToDec(unsigned char Hex)
{
	int r =0, hex = 0;

	if (Hex >= '0' && Hex <= '9')
		r = Hex - 48;
	else if (Hex >= 'a' && Hex <= 'f')
		r = Hex - 87;
	else if (Hex >= 'A' && Hex <= 'F')
		r = Hex - 55;
	hex += r * pow(16, 0);

	return hex;
};
void printRSA(RSA pq) {
	cout << "The Public Key Pair (n,e): " << pq.n << " " << pq.e << endl;
	cout << "The Private Key Pair (n,d): " << pq.n << " " << pq.d << endl;
}
RSA PQ(RSA pq, size_t size) {
	cout << "First prime number (for example 101467): ";
	cin >> pq.p;
	cout << "Second prime number (for example 103951): ";
	cin >> pq.q;	
	
	//pq.p = 101467;
	//pq.q = 103951;
	//pq.e = 14293;
	pq.n = (pq.p*1)*(pq.q*1);
	pq.f = (pq.p - 1)*1*(pq.q - 1)*1;
	cout << "Number coprime to " << pq.f << "(for example 14293): ";
	cin >> pq.e;
	
	pq.size = size;
	pq.d = getD(pq.e,pq.f);
	printRSA(pq);
	return pq;
}

int main()
{
	ifstream myfile;
	ofstream outfile;
	size_t size = 0;
	string help[8];
	PNG_info PNG_file;
	RSA pq;
	int value = 0, nextChunkLenght, actualBit;
	int ihdr, idat, iend;
	int * data1 = 0;
	char * data = 0;


	myfile.open(INPUT, ios::in | ios::binary);
	outfile.open(OUTPUT, ios::out | ios::binary);

	myfile.seekg(0,ios::end);
	size = myfile.tellg();
	myfile.seekg(0);
	data = new char [size];
	data1 = new int[size];
	myfile.read(data,size);
	bool idatawas = 0;
	for (int i = 0; i < size; i++) {
		if (data[i] == 'I' && data[i + 1] == 'H' && data[i + 2] == 'D' && data[i + 3] == 'R') { cout << "Znaleziono IHDR dla i:" << i << endl; ihdr = i; }
		if (data[i] == 'I' && data[i + 1] == 'D' && data[i + 2] == 'A' && data[i + 3] == 'T' && idatawas == 0) { cout << "Znaleziono IDAT dla i:" << i << endl; idat = i; idatawas = true; }
		if (data[i] == 'I' && data[i + 1] == 'E' && data[i + 2] == 'N' && data[i + 3] == 'D') { cout << "Znaleziono IEND dla i:" << i << endl; iend = i; }
	}
	for (int i = 0; i < 8; i++) {
			help[i] = decToHex((int)data[i]);
			for (int j = 2; j < 10; j++) {
				help[i][j] = NULL;
			}
	}		
	//if ( help[0][0] == '8' && help[0][1] == '9') { P++; }
	if( ( help[1][0] == '5' && help[1][1] == '0') &&
	( help[2][0] == '4' && help[2][1] == 'E') &&
	( help[3][0] == '4' && help[3][1] == '7') &&
	( help[4][0] == '0' && help[4][1] == 'D') &&
	( help[5][0] == '0' && help[5][1] == 'A') &&
	( help[6][0] == '1' && help[6][1] == 'A') &&
	(help[7][0] == '0' && help[7][1] == 'A')) {
		cout << "PNG Image signature was found" << endl;}
	else {
		cout<< "PNG Image signature not found" << endl;
		return 0;
	}



	int j = 3;
	actualBit = 8;
	for (int i = actualBit; i < actualBit + 4; i++)
	{
		value += pow(16, j)* data[i];
		j--;
	}
	actualBit += 4;
	nextChunkLenght = value;


	j = 7;
	actualBit += 4;
	value = 0;
	for (int i = actualBit; i < actualBit + 4; i++)
	{
		value += pow(16, j) * hexToDec(decToHex(data[i])[0]);
		j--;
		value += pow(16, j)* hexToDec(decToHex(data[i])[1]);
		j--;
	}
	PNG_file.Height = value;
	actualBit += 4;
	value = 0;



	j = 7;
	for (int i = actualBit; i < actualBit + 4; i++)
	{
		value += pow(16, j)* hexToDec(decToHex(data[i])[0]);
		j--;
		value += pow(16, j)* hexToDec(decToHex(data[i])[1]);
		j--;
	}
	actualBit += 4;
	PNG_file.Width = value;

	//BitDepth
	PNG_file.BitDepth = pow(16, 1)* hexToDec(decToHex(data[actualBit])[0]);
	PNG_file.BitDepth += pow(16, 0)* hexToDec(decToHex(data[actualBit++])[1]);
	//Color type
	PNG_file.ColorType = pow(16, 1)* hexToDec(decToHex(data[actualBit])[0]);
	PNG_file.ColorType += pow(16, 0)* hexToDec(decToHex(data[actualBit++])[1]);
	//Compression method
	PNG_file.CompressionMethod = pow(16, 1)* hexToDec(decToHex(data[actualBit])[0]);
	PNG_file.CompressionMethod += pow(16, 0)* hexToDec(decToHex(data[actualBit++])[1]);
	//Filter method
	PNG_file.FilterMethod = pow(16, 1)* hexToDec(decToHex(data[actualBit])[0]);
	PNG_file.FilterMethod += pow(16, 0)* hexToDec(decToHex(data[actualBit++])[1]);
	//Interlace Method
	PNG_file.InterlaceMethod = pow(16, 1)* hexToDec(decToHex(data[actualBit])[0]);
	PNG_file.InterlaceMethod += pow(16, 0)* hexToDec(decToHex(data[actualBit++])[1]);
	//cout << data << " ";

	

	
	losujplik(size);
	xorplik(size, data, idat, iend);
	pq=PQ(pq,size);
	encryptkey(pq,false);
	encryptkey(pq,true);
	xordeplik(size, data, idat, iend);
	zmienplik(idat, iend, data);
	outfile.write(data, size);
	outfile.close();
	myfile.close();
	
	showExif(PNG_file);
	return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
